import React from 'react';
import Addproduct from './components/Addproduct';
import Product from './components/Product';
import './App.css';

function App() {
  return (
    <div className="App">

      <Addproduct/>
      <Product/>
      
    </div>
  );
}

export default App;
