import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import { useState, useRef } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { addProducts } from '../features/productsSlice';




function Addproduct() {

    const dispatch = useDispatch();

    const impreset = useRef();
    const [data, setData] = useState(null);
    const {handleSubmit,register} = useForm();

    const onSubmit = dat =>{ 
    setData(dat);
    dispatch( addProducts(data));
    console.log(data);
    }

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        
      <Row>
        <Form.Label column="lg" lg={2}>
        name
        </Form.Label>
        <Col>
          <Form.Control ref={impreset} size="lg" type="text" placeholder="name" {...register('name')}/>
        </Col>
      </Row>
      <br />
      <Row>
        <Form.Label column lg={2}>
        price
        </Form.Label>
        <Col>
          <Form.Control ref={impreset} type="text" placeholder="...Fcfa" {...register('price')} />
        </Col>
      </Row>
      <br />
      <Row>
        <Form.Label column="sm" lg={2}>
            quantity
        </Form.Label>
        <Col>
          <Form.Control ref={impreset} size="sm" type="number" placeholder="1" {...register('quantity')}/>
        </Col>
      </Row>
      <button type='submit'> submit </button>
      </form>
    </>
  );
}

export default Addproduct;