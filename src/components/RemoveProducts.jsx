import React from 'react';

const RemoveProducts = () => {
    return (
        <div>
            <i class="fa-solid fa-trash"></i>
        </div>
    );
}

export default RemoveProducts;
