import { createSlice } from "@reduxjs/toolkit";


export const productsSlice = createSlice ({
    name : 'products',
    initialState : {
        products : null,
    },
    reducers : {
        setProducts : (state ,{payload}) => {
            state.products = payload ;
        },

        addProducts : (state ,{payload}) => {
            state.products = [...state.products , payload]
        },

        removeProducts : (state ,{payload}) => {
            state.products.filter((items)=> items.id !== payload)
        },
    }
})

export const {setProducts, addProducts, removeProducts} = productsSlice.actions;
export default productsSlice.reducer;